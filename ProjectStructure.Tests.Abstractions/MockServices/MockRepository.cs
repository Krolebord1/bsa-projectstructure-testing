﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MockQueryable.FakeItEasy;
using ProjectStructure.Domain.Interfaces;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.Tests.Abstractions.MockServices
{
    public class MockRepository<TEntity> : MockReadOnlyRepository<TEntity>, IRepository<TEntity>
        where TEntity : class, IEntity {

        private int _idCount = 1;

        public MockRepository() {}

        public MockRepository(IList<TEntity> entities) : base(entities) { }

        public IQueryable<TEntity> WriteQuery() =>
            entities.AsQueryable().BuildMock();

        public Task<TEntity?> GetAsync(int id) =>
            Task.FromResult(entities.FirstOrDefault(entity => entity.Id == id));

        public Task<IList<TEntity>> GetAllAsync() =>
            Task.FromResult(entities);

        public TEntity Add(TEntity entity)
        {
            entity.Id = _idCount;
            _idCount++;

            entities.Add(entity);

            return entity;
        }

        public TEntity Delete(TEntity entity)
        {
            entities.Remove(entity);
            return entity;
        }

        public Task<int> SaveChangesAsync() => Task.FromResult(1);
    }
}
