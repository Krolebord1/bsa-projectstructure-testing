﻿using AutoMapper;
using ProjectStructure.ApplicationServices.DTOs;
using ProjectStructure.Domain.Entities;

namespace ProjectStructure.ApplicationServices.MapperProfiles
{
    public class UsersProfile : Profile
    {
        public UsersProfile()
        {
            CreateMap<User, UserReadDTO>().ReverseMap();
            CreateMap<UserWriteDTO, User>();
        }
    }
}
