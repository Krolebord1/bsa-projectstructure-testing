﻿using System;

namespace ProjectStructure.ApplicationServices.DTOs
{
    public record UserReadDTO(
        int Id,
        int? TeamId,
        string FirstName,
        string LastName,
        string Email,
        DateTimeOffset RegisteredAt,
        DateTimeOffset BirthDay
    );
}
