﻿using System;

namespace ProjectStructure.ApplicationServices.DTOs.Teams
{
    public record TeamReadDTO(
        int Id,
        string Name,
        DateTimeOffset CreatedAt
    );
}
