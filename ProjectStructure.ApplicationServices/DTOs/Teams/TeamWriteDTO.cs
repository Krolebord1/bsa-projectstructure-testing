﻿namespace ProjectStructure.ApplicationServices.DTOs.Teams
{
    public record TeamWriteDTO(string Name);
}
