﻿using System;

namespace ProjectStructure.ApplicationServices.DTOs.Projects
{
    public record ProjectWriteDTO(
        int? AuthorId,
        int? TeamId,
        string Name,
        string Description,
        DateTimeOffset Deadline
    );
}
