﻿using MediatR;
using OneOf;
using OneOf.Types;
using ProjectStructure.ApplicationServices.DTOs.Projects;

namespace ProjectStructure.ApplicationServices.Commands.Projects
{
    public class UpdateProjectCommand : IRequest<OneOf<Success, NotFound>>
    {
        public int Id { get; }

        public ProjectWriteDTO ProjectDTO { get; }

        public UpdateProjectCommand(int id, ProjectWriteDTO projectDTO)
        {
            Id = id;
            ProjectDTO = projectDTO;
        }
    }
}
