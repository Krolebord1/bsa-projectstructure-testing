﻿
using MediatR;
using ProjectStructure.ApplicationServices.DTOs.Projects;

namespace ProjectStructure.ApplicationServices.Commands.Projects
{
    public class CreateProjectCommand : IRequest
    {
        public ProjectWriteDTO ProjectDTO { get; }

        public CreateProjectCommand(ProjectWriteDTO projectDTO)
        {
            ProjectDTO = projectDTO;
        }
    }
}
