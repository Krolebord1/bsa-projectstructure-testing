﻿using MediatR;
using OneOf;
using OneOf.Types;
using ProjectStructure.ApplicationServices.DTOs;

namespace ProjectStructure.ApplicationServices.Commands.Users
{
    public class UpdateUserCommand : IRequest<OneOf<Success, NotFound>>
    {
        public int Id { get; }

        public UserWriteDTO UserDto { get; }

        public UpdateUserCommand(int id, UserWriteDTO userDto)
        {
            Id = id;
            UserDto = userDto;
        }
    }
}
