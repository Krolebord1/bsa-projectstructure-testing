﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using OneOf;
using OneOf.Types;
using ProjectStructure.ApplicationServices.Commands.Users;
using ProjectStructure.Domain.Entities;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.ApplicationServices.Handlers.Users
{
    public class DeleteUserHandler : IRequestHandler<DeleteUserCommand, OneOf<Success, NotFound>>
    {
        private readonly IRepository<User> _usersRepository;

        public DeleteUserHandler(IRepository<User> usersRepository)
        {
            _usersRepository = usersRepository;
        }

        public async Task<OneOf<Success, NotFound>> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
        {
            var user = await _usersRepository.ReadAsync(request.Id);

            if (user == null)
                return new NotFound();

            _usersRepository.Delete(user);

            await _usersRepository.SaveChangesAsync();

            return new Success();
        }
    }
}
