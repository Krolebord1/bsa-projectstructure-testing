﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using ProjectStructure.ApplicationServices.Queries;
using ProjectStructure.Domain.Interfaces;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.ApplicationServices.Handlers.Common
{
    public abstract class GetEntitiesHandler<TEntity, TReadDTO> : IRequestHandler<GetEntitiesQuery<TReadDTO>, IEnumerable<TReadDTO>>
        where TEntity : class, IEntity
    {
        private IReadRepository<TEntity> _repository;
        private IMapper _mapper;

        public GetEntitiesHandler(IReadRepository<TEntity> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<TReadDTO>> Handle(GetEntitiesQuery<TReadDTO> request, CancellationToken cancellationToken)
        {
            var entities = await _repository.ReadAllAsync();

            return _mapper.Map<IEnumerable<TEntity>, IEnumerable<TReadDTO>>(entities);
        }
    }
}
