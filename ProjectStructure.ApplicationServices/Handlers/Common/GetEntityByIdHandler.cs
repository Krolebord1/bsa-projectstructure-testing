﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using OneOf;
using OneOf.Types;
using ProjectStructure.ApplicationServices.Queries;
using ProjectStructure.Domain.Interfaces;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.ApplicationServices.Handlers.Common
{
    public abstract class GetEntityByIdHandler<TEntity, TReadDTO> : IRequestHandler<GetEntityByIdQuery<TReadDTO>, OneOf<TReadDTO, NotFound>>
        where TEntity : class, IEntity
    {
        private IReadRepository<TEntity> _repository;
        private IMapper _mapper;

        public GetEntityByIdHandler(IReadRepository<TEntity> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<OneOf<TReadDTO, NotFound>> Handle(GetEntityByIdQuery<TReadDTO> request, CancellationToken cancellationToken)
        {
            var entity = await _repository.ReadAsync(request.Id);

            if (entity == null)
                return new NotFound();

            return _mapper.Map<TEntity, TReadDTO>(entity);
        }
    }
}
