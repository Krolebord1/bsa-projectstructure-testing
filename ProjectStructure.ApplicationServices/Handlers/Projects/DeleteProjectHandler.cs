﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using OneOf;
using OneOf.Types;
using ProjectStructure.ApplicationServices.Commands.Projects;
using ProjectStructure.Domain.Entities;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.ApplicationServices.Handlers.Projects
{
    public class DeleteProjectHandler : IRequestHandler<DeleteProjectCommand, OneOf<Success, NotFound>>
    {
        private readonly IRepository<Project> _projectsRepository;

        public DeleteProjectHandler(IRepository<Project> projectsRepository)
        {
            _projectsRepository = projectsRepository;
        }

        public async Task<OneOf<Success, NotFound>> Handle(DeleteProjectCommand request, CancellationToken cancellationToken)
        {
            var project = await _projectsRepository.ReadAsync(request.Id);

            if (project == null)
                return new NotFound();

            _projectsRepository.Delete(project);

            await _projectsRepository.SaveChangesAsync();

            return new Success();
        }
    }
}
