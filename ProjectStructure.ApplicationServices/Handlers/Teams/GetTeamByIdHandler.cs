﻿using AutoMapper;
using ProjectStructure.ApplicationServices.DTOs.Teams;
using ProjectStructure.ApplicationServices.Handlers.Common;
using ProjectStructure.Domain.Entities;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.ApplicationServices.Handlers.Teams
{
    public class GetTeamByIdHandler : GetEntityByIdHandler<Team, TeamReadDTO>
    {
        public GetTeamByIdHandler(IReadRepository<Team> repository, IMapper mapper) : base(repository, mapper) { }
    }
}
