﻿using MediatR;
using OneOf;
using OneOf.Types;

namespace ProjectStructure.ApplicationServices.Queries
{
    public class GetEntityByIdQuery<TReadDTO> : IRequest<OneOf<TReadDTO, NotFound>>
    {
        public int Id { get; }

        public GetEntityByIdQuery(int id)
        {
            Id = id;
        }
    }
}
