﻿using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ProjectStructure.ApplicationServices.DTOs.Teams;
using Xunit;

namespace ProjectStructure.Tests.Integration.Controllers
{
    public class TeamsControllerTests : IClassFixture<AppFactory>
    {
        private readonly HttpClient _client;

        public TeamsControllerTests(AppFactory factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public async Task AddValidTeam_ThenResponseWithCode201()
        {
            var teamDto = new TeamWriteDTO("g2");

            var json = JsonConvert.SerializeObject(teamDto);

            var response = await _client.PostAsync("api/teams", new StringContent(json, Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }

        [Fact]
        public async Task AddInvalidTeam_ThenResponseWithCode400()
        {
            var teamDto = new {Please = "Let Us Create New Team"};

            var json = JsonConvert.SerializeObject(teamDto);

            var response = await _client.PostAsync("api/teams", new StringContent(json, Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
