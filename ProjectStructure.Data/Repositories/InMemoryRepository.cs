﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructure.Data.Interfaces;
using ProjectStructure.Domain.Interfaces;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.Data.Repositories
{
    public class InMemoryRepository<TEntity> : InMemoryReadonlyRepository<TEntity>, IRepository<TEntity>
        where TEntity : class, IEntity
    {
        private readonly IDbContext _context;

        private int _idCount = 1;

        public InMemoryRepository(IDbContext context) : base(context)
        {
            _context = context;
        }

        public IQueryable<TEntity> WriteQuery() =>
            entities.AsQueryable();

        public Task<TEntity?> GetAsync(int id) =>
            Task.FromResult(entities.FirstOrDefault(entity => entity.Id == id));

        public Task<IList<TEntity>> GetAllAsync() =>
            Task.FromResult(entities);

        public TEntity Add(TEntity entity)
        {
            entity.Id = _idCount;
            _idCount++;

            entities.Add(entity);

            return entity;
        }

        public TEntity Delete(TEntity entity)
        {
            entities.Remove(entity);
            return entity;
        }

        public Task<int> SaveChangesAsync() =>
            _context.SaveChangesAsync();
    }
}
