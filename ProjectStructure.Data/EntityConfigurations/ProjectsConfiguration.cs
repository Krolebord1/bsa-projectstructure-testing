﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProjectStructure.Data.EntityConfigurationExtensions;
using ProjectStructure.Domain.Entities;

namespace ProjectStructure.Data.EntityConfigurations
{
    public class ProjectsConfiguration : IEntityTypeConfiguration<Project>
    {
        public void Configure(EntityTypeBuilder<Project> builder)
        {
            builder.ToTable("Projects");

            builder.HasKey(project => project.Id);

            builder.Property(project => project.Name)
                .IsName();

            builder.Property(project => project.Description)
                .IsNonNullableString();

            builder
                .HasOne(project => project.Author)
                .WithMany()
                .HasForeignKey(project => project.AuthorId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .HasOne(project => project.Team)
                .WithMany(team => team!.Projects)
                .HasForeignKey(project => project.TeamId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);
        }
    }
}
