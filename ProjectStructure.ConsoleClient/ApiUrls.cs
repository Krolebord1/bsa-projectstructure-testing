﻿namespace ProjectStructure.ConsoleClient
{
    public static class ApiUrls
    {
        public static readonly string Scheme = "https";
        public static readonly string Authority = "localhost:5001";
        public static readonly string ApiPath = "api";

        public static readonly string FullApiPath = $"{Scheme}://{Authority}/{ApiPath}";

        public static readonly CrudControllerUrls Users = new CrudControllerUrls("users");

        public static readonly CrudControllerUrls Projects = new CrudControllerUrls("projects");

        public static readonly CrudControllerUrls Teams = new CrudControllerUrls("teams");

        public static readonly CrudControllerUrls Tasks = new CrudControllerUrls("tasks");

        public static class Selections
        {
            public static string GetUserTasksCount(int userId) =>
                $"{FullApiPath}/selections/users/{userId}/tasks/count";

            public static string GetUserTasks(int userId) =>
                $"{FullApiPath}/selections/users/{userId}/tasks";

            public static string GetUserTasksFinishedInCurrentYear(int userId) =>
                $"{FullApiPath}/selections/users/{userId}/tasks/finished-in-current-year";

            public static string GetTeamsWithOldEnoughUsers() =>
                $"{FullApiPath}/selections/teams/old-enough-users";

            public static string GetSortedUsers() =>
                $"{FullApiPath}/selections/users/sorted";

            public static string GetUserSummary(int userId) =>
                $"{FullApiPath}/selections/users/{userId}/summary";

            public static string GetProjectSummary() =>
                $"{FullApiPath}/selections/projects/summaries";
        }

        public sealed class CrudControllerUrls
        {
            private readonly string _controllerPath;

            public CrudControllerUrls(string controller)
            {
                _controllerPath = $"{FullApiPath}/{controller}";
            }

            public string GetAll() => _controllerPath;

            public string GetById(int id) => $"{_controllerPath}/{id}";

            public string PostNew() => _controllerPath;

            public string PutUpdate(int id) => $"{_controllerPath}/{id}";

            public string Delete(int id) => $"{_controllerPath}/{id}";
        }
    }
}
