﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace ProjectStructure.ConsoleClient.Services
{
    public interface IDataLoader : IDisposable
    {
        public Task<T?> GetDeserializedAsync<T>(string uri);

        public Task<HttpContent> GetContentAsync(string uri);
    }
}
