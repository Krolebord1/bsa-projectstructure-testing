﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ProjectStructure.ConsoleClient.Services
{
    public class DataLoader : IDataLoader
    {
        private readonly HttpClient _client;

        public DataLoader()
        {
            _client = new HttpClient();
        }

        public async Task<T?> GetDeserializedAsync<T>(string uri)
        {
            var content = await GetContentAsync(uri);

            var json = await content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<T>(json);
        }

        public async Task<HttpContent> GetContentAsync(string uri)
        {
            try
            {
                var response = await _client.GetAsync(uri);

                if (!response.IsSuccessStatusCode)
                    throw new DataLoadingException($"Response status code is: {response.StatusCode}");

                return response.Content;
            }
            catch (HttpRequestException e)
            {
                throw new DataLoadingException("HttpException has been thrown", e);
            }
        }

        public void Dispose()
        {
            _client.Dispose();
        }
    }

    public class DataLoadingException : Exception
    {
        public DataLoadingException(string message, HttpRequestException? httpException = default)
            : base(message, httpException) {}
    }
}
