﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using ProjectStructure.ConsoleClient.Attributes;
using ProjectStructure.ConsoleClient.Interfaces;

namespace ProjectStructure.ConsoleClient.Programs
{
    public class ConsoleProgramBase : AsyncProgramBase
    {
        public override async Task RunAsync()
        {
            List<KeyValuePair<string, IAsyncProgram>> programs = new();

            var type = GetType();
            foreach (var methodInfo in type.GetMethods())
            {
                var entryAttribute = methodInfo.GetCustomAttribute<ProgramEntryAttribute>();

                if(entryAttribute == null)
                    continue;

                programs.Add(new(
                    entryAttribute.Path,
                    new DelegateProgram((Func<Task>)Delegate.CreateDelegate(typeof(Func<Task>), this, methodInfo)))
                );
            }

            var navigator = new NavigatorProgram(programs);
            await navigator.RunAsync();
        }
    }
}
