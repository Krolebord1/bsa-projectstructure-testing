﻿using System;
using System.Threading.Tasks;
using ProjectStructure.ConsoleClient.Interfaces;

namespace ProjectStructure.ConsoleClient.Programs
{
    public abstract class AsyncProgramBase : IAsyncProgram
    {
        public abstract Task RunAsync();

        protected void WaitForKey()
        {
            Console.WriteLine();
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();
        }

        protected void WriteError(string message = "")
        {
            Console.WriteLine();
            Console.WriteLine("Error occurred");

            if (!string.IsNullOrWhiteSpace(message))
            {
                Console.WriteLine();
                Console.WriteLine(message);
            }

            WaitForKey();
        }
    }
}
